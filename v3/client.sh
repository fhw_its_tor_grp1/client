#!/bin/bash

TOR_HOSTNAME="${1}"
CAPTURE_NAME="${2}"
NUM_REQUESTS=$(($RANDOM % 5 + 1))

echo "Starting capture"
tshark -i eth0 -f "tcp && ip && not arp && not icmp && not icmp6" -w ${CAPTURE_NAME} &

sleep 1

echo "Starting client" > "${CAPTURE_NAME}.log"

for (( i=1; i <= $NUM_REQUESTS; i++ )); do
  printf "Request %d/%d to " $i $NUM_REQUESTS
  ./request.sh $TOR_HOSTNAME >> "${CAPTURE_NAME}.log"
done
echo -ne "\n"

sleep 1

echo "Client finished, terminating"
kill $(jobs -p)
pkill tor

sleep 1
