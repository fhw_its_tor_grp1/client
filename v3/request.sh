#!/bin/bash

TOR_HOSTNAME="${1}"

# Generate a random number from 0-5
# 4 numbers lead to request to the hidden service
# 2 numbers lead to noise
# => 2/3 chance to request the hidden service
case $(($RANDOM % 6)) in
    0)
       URL="http://${TOR_HOSTNAME}/zero_1m" 
       ;;
    1)
       URL="http://${TOR_HOSTNAME}/zero_2m" 
       ;;
    2)
       URL="http://${TOR_HOSTNAME}/zero_5m" 
       ;;
    3)
       URL="http://${TOR_HOSTNAME}/zero_10m" 
       ;;
    *)
       URL="http://duckduckgo.com" 
       ;;
esac

echo $URL
curl -s --socks5-hostname localhost:7000 ${URL} > /dev/null

sleep 1

