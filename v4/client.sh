#!/bin/bash

TOR_HOSTNAME="${1}"
INPUT_FILE="${2}"
CAPTURE_NAME="${3}"

echo "Starting capture"
tshark -i eth0 -f "tcp && ip && not arp && not icmp && not icmp6" -w ${CAPTURE_NAME} &

sleep 1

for i in $(cat $INPUT_FILE); do 
  URL=${i/HOST/$TOR_HOSTNAME}
  curl -s --socks5-hostname localhost:7000 ${URL} > /dev/null
done

sleep 1

echo "Client finished, terminating"
kill $(jobs -p)
pkill tor

sleep 1
