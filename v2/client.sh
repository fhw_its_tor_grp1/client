#!/bin/bash

TOR_HOSTNAME="${1}"
TOR_FILEPATH="${2}"
NUM_REQUESTS="${3}"
CAPTURE_NAME="${4}"

echo "Starting capture"
tshark -i eth0 -f "tcp && ip && not arp && not icmp && not icmp6" -w ${CAPTURE_NAME} &

sleep 1

echo "Starting client"

for (( i=1; i <= $NUM_REQUESTS; i++ )); do
  curl -s --socks5-hostname localhost:7000 http://${TOR_HOSTNAME}/${TOR_FILEPATH:-} > /dev/null
  echo -ne "${i}/${NUM_REQUESTS}\r"
done
echo -ne "\n"

sleep 1

kill $(jobs -p)
pkill tor

sleep 1
