#!/bin/bash

NUM_REQUESTS=${NUM_REQUESTS:=20}

wait_str() {
  local file="$1"; shift
  local search_term="$1"; shift
  local wait_time="${1:-5m}"; shift # 5 minutes as default timeout

  (timeout $wait_time tail -F -n+0 "$file" &) | grep -q "$search_term" && return 0

  echo "Timeout of $wait_time reached. Unable to find '$search_term' in '$file'"
  return 1
}

echo "Starting tor"
tor -f /etc/tor/torrc &

sleep 1

wait_str /var/lib/tor/logfile "Bootstrapped 100%" && \
echo "Tor is up"
echo "Sending request to google to warm up tor client"

curl -s --socks5-hostname localhost:7000 http://google.de > /dev/null
sleep 1

echo "Starting capture"
tshark -i eth0 -f "tcp && ip && not arp && not icmp && not icmp6" -w /data/${CAPTURE_NAME}.pcap &

sleep 1

echo "Starting client"

for (( i=1; i<=${NUM_REQUESTS}; i++ )); do
  curl -s --socks5-hostname localhost:7000 http://${TOR_HOSTNAME}/${TOR_FILEPATH:-} > /dev/null
  echo -ne "${i}/${NUM_REQUESTS}\r"
done
echo -ne "\n"

sleep 1

echo "Client finished, terminating"
kill $(jobs -p)

sleep 1